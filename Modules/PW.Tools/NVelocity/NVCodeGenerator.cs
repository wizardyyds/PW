﻿using NVelocity;
using NVelocity.App;
using NVelocity.Runtime;
using PW.Tools.Views;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PW.Tools.NVelocity
{
    public class NVCodeGenerator
    {
        private VelocityEngine vltEngine;
        public string TempPath;
        public string CodePath;
        public void InitTemplateSetting() {
            // 初始化模板引擎及设置模板读取路径
            //  AppDomain.CurrentDomain.BaseDirectory + "template/mode.xslt", AppDomain.CurrentDomain.BaseDirectory + "CodeGenerator/model/" + name + ".cs"

            TempPath = AppDomain.CurrentDomain.BaseDirectory + "/template";
            DirectoryInfo _TempPath = new DirectoryInfo(TempPath);
            if (!_TempPath.Exists)
            {
                _TempPath.Create();
            }
            CodePath = AppDomain.CurrentDomain.BaseDirectory + "/CodeGenerator";
            DirectoryInfo _CodePath = new DirectoryInfo(CodePath);
            if (!_CodePath.Exists)
            {
                _CodePath.Create();
            }

            vltEngine = new VelocityEngine();
            vltEngine.SetProperty(RuntimeConstants.RESOURCE_LOADER, "file");
            vltEngine.SetProperty(RuntimeConstants.FILE_RESOURCE_LOADER_PATH, TempPath);
            vltEngine.Init();
        }
        public String GeneratemeplateFile(TableModel table, String template, String outFileName)
        {
            InitTemplateSetting();
            string CodeFilePath = CodePath + "/" + outFileName;
            DirectoryInfo dir = new DirectoryInfo(CodeFilePath);
            if (!Directory.Exists(dir.Parent.FullName))//如果不存在就创建file文件夹
            {
                Directory.CreateDirectory(dir.Parent.FullName);
            }

            VelocityContext vltContext = tableToVelocityContext(table);
            Template vltTemplate = vltEngine.GetTemplate(template);
            StringWriter vltWriter = new StringWriter();
            vltTemplate.Merge(vltContext, vltWriter);
            string CodeContent = vltWriter.GetStringBuilder().ToString();

            //保存生成后的代码内容到文件
            saveFile(CodeFilePath, CodeContent);
            return CodeFilePath;
        }

        public VelocityContext tableToVelocityContext(TableModel table) {
            VelocityContext vltContext = new VelocityContext();
            vltContext.Put("TableName", table.TableName);
            vltContext.Put("NameSpace", table.NameSpace);
            vltContext.Put("ModelName", table.ModelName);
            vltContext.Put("DbContextName", table.DbContextName);
            vltContext.Put("Comment", table.Comment);
            vltContext.Put("Fields", table.Fields);
            return vltContext;
        }

        public void saveFile(string filePath, string content) {
            StreamWriter file = new StreamWriter(filePath, false);
            file.Write(content);
            file.Close();
            file.Dispose();
        }
    }
}
