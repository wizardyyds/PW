﻿
  using PW.SystemSet.ViewModel;
  using System.ComponentModel.Composition;
  using System.Windows;
  using System.Windows.Controls;

  namespace PW.SystemSet.Views
  {
      [Export(typeof(SysUserView))]
      public partial class SysUserView : UserControl
      {
          public SysUserView()
          {
            InitializeComponent();
            this.DataContext = new SysUserListViewModel();
        }

          private void UserControl_Loaded(object sender, RoutedEventArgs e)
          {
          }
      }
  }
