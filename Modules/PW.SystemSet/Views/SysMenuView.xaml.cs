using PW.SystemSet.ViewModel;
  using System.ComponentModel.Composition;
  using System.Windows;
  using System.Windows.Controls;

  namespace PW.SystemSet.Views
  {
      [Export(typeof(SysMenuView))]
      public partial class SysMenuView : UserControl
      {
          public SysMenuView()
          {
              InitializeComponent();
              this.DataContext = new SysMenuListViewModel();
          }

          private void UserControl_Loaded(object sender, RoutedEventArgs e)
          {
          }
      }
  }