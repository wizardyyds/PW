using PW.SystemSet.ViewModel;
  using System.ComponentModel.Composition;
  using System.Windows;
  using System.Windows.Controls;

  namespace PW.SystemSet.Views
  {
      [Export(typeof(SysConfigView))]
      public partial class SysConfigView : UserControl
      {
          public SysConfigView()
          {
              InitializeComponent();
              this.DataContext = new SysConfigListViewModel();
          }

          private void UserControl_Loaded(object sender, RoutedEventArgs e)
          {
          }
      }
  }