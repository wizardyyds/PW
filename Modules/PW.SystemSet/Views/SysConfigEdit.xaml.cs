using PW.Controls.Controls;
using PW.ServiceCenter.ServiceSysConfig;
using PW.SystemSet.ViewModel;
using System.Windows;

namespace PW.SystemSet.Views
{
    /// <summary>
    /// SysConfigEdit.xaml 的交互逻辑
    /// </summary>
    public partial class SysConfigEdit : WindowBase
    {
        sys_config record = null;
        bool disabled = false;
        public SysConfigEdit(sys_config record, bool disabled)
        {
            this.record = record;
            InitializeComponent();
            this.DataContext = new SysConfigViewModel(record, disabled);
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
        }
    }
}
