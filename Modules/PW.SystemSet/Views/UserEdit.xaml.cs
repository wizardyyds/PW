﻿using PW.Controls.Controls;
using PW.ServiceCenter.ServiceSysUser;
using PW.SystemSet.ViewModel;
using System.Windows;

namespace PW.SystemSet.Views
{
    /// <summary>
    /// UserEdit.xaml 的交互逻辑
    /// </summary>
    public partial class UserEdit : WindowBase
    {
        sys_user record = null;
        bool disabled = false;
        public UserEdit(sys_user record, bool disabled)
        {
            this.record = record;
            InitializeComponent();
            this.DataContext = new SysUserViewModel(record, disabled);
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
        }
    }
}
