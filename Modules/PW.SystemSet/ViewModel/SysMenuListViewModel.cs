using PW.Common;
using PW.ServiceCenter;
using PW.ServiceCenter.ServiceSysMenu;
using PW.SystemSet.Views;
using System;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Windows;
using System.Linq;
using System.Collections.Generic;

  namespace PW.SystemSet.ViewModel
  {
  
  public class SysMenuListViewModel : INotifyPropertyChanged
  {
      //构造函数
      public SysMenuListViewModel()
      {
          QueryCommand = new RelayCommand(QueryCommandFunc);
          NextPageSearchCommand = new RelayCommand(NextPageSearchCommandFunc);
          SelectAllCommand = new RelayCommand(SelectAllCommandFunc);
          UnSelectAllCommand = new RelayCommand(UnSelectAllCommandFunc);
          AddCommand = new RelayCommand(AddCommandFunc);
          DeleteCommand = new RelayCommand(DeleteCommandFunc);
          ModifyCommand = new RelayCommand(ModifyCommandFunc);
          InfoCommand = new RelayCommand(InfoCommandFunc);
          GetData();
      }

      #region 查询相关属性
      private sys_menu _queryObj = new sys_menu();
      /// <summary>
      /// Username
      /// </summary>
      public sys_menu QueryObj
      {
        get { return _queryObj; }
        set
        {
          _queryObj = value;
          RaisePropertyChanged("QueryObj");
        }
      }

      public RelayCommand QueryCommand { get; set; }

      private void QueryCommandFunc()
      {
        CurrentPage = "1";
        GetData();
      }

      //数据源
      ObservableCollection<DataGridModel<sys_menu>> _list = new ObservableCollection<DataGridModel<sys_menu>>();
      public ObservableCollection<DataGridModel<sys_menu>> list
      {
        get { return _list; }
        set
        {
          _list = value;
          RaisePropertyChanged("list");
        }
      }
        private void GetData()
        {
        var pageIndex = Convert.ToInt32(CurrentPage);
        CServiceSysMenu client = new CServiceSysMenu();
        client.queryPageCompleted += (serice, eve) =>
        {
        if (eve.Succesed)
        {
        list.Clear();
        PageInfoOfsys_menuCLUigIiY result = eve.Result;
        this.TotalPage = result.totalPage.Value;
        this.TotalCount = result.totalCount == null ? 0 : result.totalCount.Value;
        foreach (sys_menu item in result.list)
        {
        list.Add(new DataGridModel<sys_menu>() { IsChecked = false, ObjData = item });
          }
          }
          else
          {
          }
          };
          PageInfoOfsys_menuCLUigIiY page = new PageInfoOfsys_menuCLUigIiY()
          {
          pageIndex = pageIndex,
          pageSize = PageSize,
          orderName = "create_time desc",
          queryParams = _queryObj
          };
          client.queryPage(JSONCom.ConvertObject<PageInfoOfsys_menuCLUigIiY>(page));
            }
            #endregion

                #region 分页相关属性
                /// <summary>
                  /// 分页管理
                  ///</summary>
                public RelayCommand NextPageSearchCommand { get; set; }
                /// <summary>
                  /// 分页查询命令
                  ///</summary>
                private void NextPageSearchCommandFunc()
                {
                GetData();
                }
                private int _totalPage = 0;
                /// <summary>
                  /// 总页数
                  ///</summary>
                public int TotalPage
                {
                get { return _totalPage; }
                set
                {
                _totalPage = value;
                RaisePropertyChanged("TotalPage");
                }
                }

                private string _currentPage = "1";
                /// <summary>
                  /// 当前页
                  /// </summary>
                public string CurrentPage
                {
                get { return _currentPage; }
                set
                {
                _currentPage = value;
                RaisePropertyChanged("CurrentPage");
                }
                }

                private int _pageSize = 10;
                /// <summary>
                  /// 每页显示的记录数
                  ///</summary>
                public int PageSize
                {
                get { return _pageSize; }
                set
                {
                _pageSize = value;
                RaisePropertyChanged("PageSize");
                }
                }
                //private int _pageIndex = 1;
                //public int PageIndex
                //{
                //    get { return _pageIndex; }
                //    set
                //    {
                //        _pageIndex = value;

                //        RaisePropertyChanged("PageIndex");
                //    }
                //}
                private int _totalCount;
                public int TotalCount
                {
                get { return _totalCount; }
                set
                {
                _totalCount = value;
                RaisePropertyChanged("TotalCount");
                }
                }
                #endregion

                #region datagrid全选
                private bool? _IsCheckedAll = false;
                /// <summary>
                  ///
                  ///</summary>
                public bool? IsCheckedAll
                {
                get { return _IsCheckedAll; }
                set
                {
                _IsCheckedAll = value;
                RaisePropertyChanged("IsCheckedAll");
                }
                }

                public RelayCommand SelectAllCommand { get; set; }
                private void SelectAllCommandFunc()
                {
                foreach (DataGridModel<sys_menu> item in list)
                  {
                  item.IsChecked = true;
                  }
                  }
                  public RelayCommand UnSelectAllCommand { get; set; }
                  private void UnSelectAllCommandFunc()
                  {
                  foreach (DataGridModel<sys_menu> item in list)
                {
                    item.IsChecked = false;
                }
            }
            #endregion

            #region 增删查改命令
            public RelayCommand AddCommand { get; set; }
            private void AddCommandFunc()
            {
                SysMenuEdit edit = new SysMenuEdit(new sys_menu(), false);
                edit.ShowDialog();
            }
            public RelayCommand DeleteCommand { get; set; }
            private void DeleteCommandFunc()
            {
               sys_menu record = getSelectRecord();
				if (record == null)
				{
					MessageBoxX.Warning("请选择一条数据");
					return;
				}
				// 支持多选
				bool res = MessageBoxX.Question("确认删除所选数据？");
				if (res) {
					CServiceSysMenu client = new CServiceSysMenu();
					client.deleteByIdCompleted += (serice, eve) =>
					{
						if (eve.Succesed)
						{
							MessageBoxX.Success("删除成功！");
						}
						else
						{
							MessageBoxX.Error("删除失败！");
						}
					};
					 					  					  client.deleteById(record.menu_id);
					  					  					  					  					  					  					  					  					  					  					  					  					  					  					  					  					  					  					  					  					  					  					  					  					  					  					  					  					  					  					  					  					  					  					  					  					
				}
            }
            public RelayCommand ModifyCommand { get; set; }
            private void ModifyCommandFunc()
            {
				sys_menu record = getSelectRecord();
				if (record == null)
				{
					MessageBoxX.Warning("请选择一条数据");
					return;
				}
                SysMenuEdit edit = new SysMenuEdit(record, false);
                edit.ShowDialog();
            }
            public RelayCommand InfoCommand { get; set; }
            private void InfoCommandFunc()
            {
				sys_menu record = getSelectRecord();
				if (record == null)
				{
					MessageBoxX.Warning("请选择一条数据");
					return;
				}
                SysMenuEdit edit = new SysMenuEdit(record, true);
                edit.ShowDialog();
            }

			 private sys_menu getSelectRecord() {
            List<DataGridModel<sys_menu>> records = _list.Where(item => item.IsChecked).ToList();
            if (records != null && records.Count > 0)
            {
                return records[0].ObjData;
            }
            return null;
			}
			private List<DataGridModel<sys_menu>> getSelectRecords()
			{
				List<DataGridModel<sys_menu>> records = _list.Where(item => item.IsChecked).ToList();
				return records;
			}
            #endregion

            public event PropertyChangedEventHandler PropertyChanged;

            public void RaisePropertyChanged(string propertyName)
            {
                if (PropertyChanged != null)
                {
                    PropertyChanged.Invoke(this, new PropertyChangedEventArgs(propertyName));
                }
            }
      }
  }