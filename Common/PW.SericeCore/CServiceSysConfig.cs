﻿using PW.Infrastructure;
using PW.ServiceCenter.ServiceSysConfig;

namespace PW.ServiceCenter
{
    public class CServiceSysConfig
    {
        #region 查询
        public event System.EventHandler<ServicesEventArgs<sys_config[]>> queryCompleted;
        public void query(sys_config record)
        {
            ServiceSysConfigClient client = new ServiceSysConfigClient();
            client.queryCompleted += (sender, e) =>
            {
                ServicesEventArgs<sys_config[]> arg = new ServicesEventArgs<sys_config[]>();

                if (e.Error == null)
                {
                    arg.Result = e.Result;
                    arg.Succesed = true;
                }
                else
                {
                    arg.Succesed = false;
                    arg.Error = e.Error;
                }
                if (queryCompleted != null)
                {
                    queryCompleted.Invoke(this, arg);
                }
            };
            client.queryAsync(record);
        }
        #endregion

        #region 分页查询
        public event System.EventHandler<ServicesEventArgs<PageInfoOfsys_configCLUigIiY>> queryPageCompleted;
        public void queryPage(PageInfoOfsys_configCLUigIiY page)
        {
            ServiceSysConfigClient client = new ServiceSysConfigClient();
            client.queryPageCompleted += (sender, e) =>
            {
                ServicesEventArgs<PageInfoOfsys_configCLUigIiY> arg = new ServicesEventArgs<PageInfoOfsys_configCLUigIiY>();

                if (e.Error == null)
                {
                    arg.Result = e.Result;
                    arg.Succesed = true;
                }
                else
                {
                    arg.Succesed = false;
                    arg.Error = e.Error;
                }
                if (queryPageCompleted != null)
                {
                    queryPageCompleted.Invoke(this, arg);
                }
            };
            client.queryPageAsync(page);
        }
        #endregion

        #region 删除
        public event System.EventHandler<ServicesEventArgs<int>> deleteByIdCompleted;
        public void deleteById(int config_id)
        {
            ServiceSysConfigClient client = new ServiceSysConfigClient();
            client.deleteByIdCompleted += (sender, e) =>
            {
                ServicesEventArgs<int> arg = new ServicesEventArgs<int>();

                if (e.Error == null)
                {
                    arg.Result = e.Result;
                    arg.Succesed = e.Result > 0;
                }
                else
                {
                    arg.Succesed = false;
                    arg.Error = e.Error;
                }
                if (deleteByIdCompleted != null)
                {
                    deleteByIdCompleted.Invoke(this, arg);
                }
            };
            client.deleteByIdAsync(config_id);
        }
        #endregion

        #region 添加
        public event System.EventHandler<ServicesEventArgs<int>> addCompleted;
        public void add(sys_config record)
        {
            ServiceSysConfigClient client = new ServiceSysConfigClient();
            client.addCompleted += (sender, e) =>
            {
                ServicesEventArgs<int> arg = new ServicesEventArgs<int>();

                if (e.Error == null)
                {
                    arg.Result = e.Result;
                    arg.Succesed = e.Result > 0;
                }
                else
                {
                    arg.Succesed = false;
                    arg.Error = e.Error;
                }
                if (addCompleted != null)
                {
                    addCompleted.Invoke(this, arg);
                }
            };
            client.addAsync(record);
        }
        #endregion
        #region 更新
        public event System.EventHandler<ServicesEventArgs<int>> updateByIdCompleted;
        public void updateById(sys_config record)
        {
            ServiceSysConfigClient client = new ServiceSysConfigClient();
            client.updateByIdCompleted += (sender, e) =>
            {
                ServicesEventArgs<int> arg = new ServicesEventArgs<int>();

                if (e.Error == null)
                {
                    arg.Result = e.Result;
                    arg.Succesed = e.Result > 0;
                }
                else
                {
                    arg.Succesed = false;
                    arg.Error = e.Error;
                }
                if (updateByIdCompleted != null)
                {
                    updateByIdCompleted.Invoke(this, arg);
                }
            };
            client.updateByIdAsync(record);
        }
        #endregion
        #region 详情
        public event System.EventHandler<ServicesEventArgs<sys_config>> getByIdCompleted;
        public void getById(int config_id)

        {
            ServiceSysConfigClient client = new ServiceSysConfigClient();
            client.getByIdCompleted += (sender, e) =>
            {
                ServicesEventArgs<sys_config> arg = new ServicesEventArgs<sys_config>();

                if (e.Error == null)
                {
                    arg.Result = e.Result;
                    arg.Succesed = true;
                }
                else
                {
                    arg.Succesed = false;
                    arg.Error = e.Error;
                }
                if (getByIdCompleted != null)
                {
                    getByIdCompleted.Invoke(this, arg);
                }
            };
            client.getByIdAsync(config_id);
        }
        #endregion
    }
}