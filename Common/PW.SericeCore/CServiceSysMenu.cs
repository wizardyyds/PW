using PW.Infrastructure;
using PW.ServiceCenter.ServiceSysMenu;

namespace PW.ServiceCenter
{
    public class CServiceSysMenu
    {
        #region 查询
        public event System.EventHandler<ServicesEventArgs<sys_menu[]>> queryCompleted;
        public void query(sys_menu record)
        {
            ServiceSysMenuClient client = new ServiceSysMenuClient();
            client.queryCompleted += (sender, e) =>
            {
                ServicesEventArgs<sys_menu[]> arg = new ServicesEventArgs<sys_menu[]>();

                if (e.Error == null)
                {
                    arg.Result = e.Result;
                    arg.Succesed = true;
                }
                else
                {
                    arg.Succesed = false;
                    arg.Error = e.Error;
                }
                if (queryCompleted != null)
                {
                    queryCompleted.Invoke(this, arg);
                }
            };
            client.queryAsync(record);
        }
        #endregion

        #region 分页查询
        public event System.EventHandler<ServicesEventArgs<PageInfoOfsys_menuCLUigIiY>> queryPageCompleted;
        public void queryPage(PageInfoOfsys_menuCLUigIiY page)
        {
            ServiceSysMenuClient client = new ServiceSysMenuClient();
            client.queryPageCompleted += (sender, e) =>
            {
                ServicesEventArgs<PageInfoOfsys_menuCLUigIiY> arg = new ServicesEventArgs<PageInfoOfsys_menuCLUigIiY>();

                if (e.Error == null)
                {
                    arg.Result = e.Result;
                    arg.Succesed = true;
                }
                else
                {
                    arg.Succesed = false;
                    arg.Error = e.Error;
                }
                if (queryPageCompleted != null)
                {
                    queryPageCompleted.Invoke(this, arg);
                }
            };
            client.queryPageAsync(page);
        }
        #endregion

		 #region 删除
        public event System.EventHandler<ServicesEventArgs<int>> deleteByIdCompleted;
						 public void deleteById(long menu_id)
																																																																								        {
            ServiceSysMenuClient client = new ServiceSysMenuClient();
            client.deleteByIdCompleted += (sender, e) =>
            {
                ServicesEventArgs<int> arg = new ServicesEventArgs<int>();

                if (e.Error == null)
                {
                    arg.Result = e.Result;
                    arg.Succesed = e.Result > 0;
                }
                else
                {
                    arg.Succesed = false;
                    arg.Error = e.Error;
                }
                if (deleteByIdCompleted != null)
                {
                    deleteByIdCompleted.Invoke(this, arg);
                }
            };
									 client.deleteByIdAsync(menu_id);
																																																																																																												        }
        #endregion

        #region 添加
        public event System.EventHandler<ServicesEventArgs<int>> addCompleted;
        public void add(sys_menu record)
        {
            ServiceSysMenuClient client = new ServiceSysMenuClient();
            client.addCompleted += (sender, e) =>
            {
                ServicesEventArgs<int> arg = new ServicesEventArgs<int>();

                if (e.Error == null)
                {
                    arg.Result = e.Result;
                    arg.Succesed = e.Result > 0;
                }
                else
                {
                    arg.Succesed = false;
                    arg.Error = e.Error;
                }
                if (addCompleted != null)
                {
                    addCompleted.Invoke(this, arg);
                }
            };
            client.addAsync(record);
        }
        #endregion
        #region 更新
        public event System.EventHandler<ServicesEventArgs<int>> updateByIdCompleted;
        public void updateById(sys_menu record)
        {
            ServiceSysMenuClient client = new ServiceSysMenuClient();
            client.updateByIdCompleted += (sender, e) =>
            {
                ServicesEventArgs<int> arg = new ServicesEventArgs<int>();

                if (e.Error == null)
                {
                    arg.Result = e.Result;
                    arg.Succesed = e.Result > 0;
                }
                else
                {
                    arg.Succesed = false;
                    arg.Error = e.Error;
                }
                if (updateByIdCompleted != null)
                {
                    updateByIdCompleted.Invoke(this, arg);
                }
            };
            client.updateByIdAsync(record);
        }
        #endregion
        #region 详情
        public event System.EventHandler<ServicesEventArgs<sys_menu>> getByIdCompleted;
						 public void getById(long menu_id)
																																																																								        
        {
            ServiceSysMenuClient client = new ServiceSysMenuClient();
            client.getByIdCompleted += (sender, e) =>
            {
                ServicesEventArgs<sys_menu> arg = new ServicesEventArgs<sys_menu>();

                if (e.Error == null)
                {
                    arg.Result = e.Result;
                    arg.Succesed = true;
                }
                else
                {
                    arg.Succesed = false;
                    arg.Error = e.Error;
                }
                if (getByIdCompleted != null)
                {
                    getByIdCompleted.Invoke(this, arg);
                }
            };
						            client.getByIdAsync(menu_id);
																																																																																																												        }
        #endregion
    }
}