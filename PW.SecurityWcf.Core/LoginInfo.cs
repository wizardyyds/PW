﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace PW.SecurityWcf.Core
{
    public static class LoginInfo
    {
        public static long UserId { get; set; }

        public static string UserName { get; set; }

        public static string Password { get; set; }

        public static string Token { get; set; }

        public static bool IsRemember { get; set; }
    }
}
