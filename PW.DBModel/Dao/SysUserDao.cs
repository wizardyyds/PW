﻿
  using PW.DBCommon.Model;
  using System;
  using System.Collections.Generic;
  using System.Data.Entity;
  using System.Linq;
  using System.Linq.Expressions;

  namespace PW.DBCommon.Dao
  {
  
  public class SysUserDao : BaseDao
    {
        /// <summary>
        /// 查询
        /// </summary>
        public List<sys_user> query(sys_user record)
        {
            using (pwEntities myDb = new pwEntities())
            {
                IQueryable<sys_user> db = myDb.sys_user;
                if (record != null)
                {
                
                    if (record.user_id != null)
                    
                    {
                        db = db.Where<sys_user>(p => p.user_id.Equals(record.user_id));
                    }
                
                    if (record.dept_id != null)
                    
                    {
                        db = db.Where<sys_user>(p => p.dept_id.Equals(record.dept_id));
                    }
                
                    if (!String.IsNullOrEmpty(record.user_name))
                    
                    {
                        db = db.Where<sys_user>(p => p.user_name.Equals(record.user_name));
                    }
                
                    if (!String.IsNullOrEmpty(record.nick_name))
                    
                    {
                        db = db.Where<sys_user>(p => p.nick_name.Equals(record.nick_name));
                    }
                
                    if (!String.IsNullOrEmpty(record.user_type))
                    
                    {
                        db = db.Where<sys_user>(p => p.user_type.Equals(record.user_type));
                    }
                
                    if (!String.IsNullOrEmpty(record.email))
                    
                    {
                        db = db.Where<sys_user>(p => p.email.Equals(record.email));
                    }
                
                    if (!String.IsNullOrEmpty(record.phonenumber))
                    
                    {
                        db = db.Where<sys_user>(p => p.phonenumber.Equals(record.phonenumber));
                    }
                
                    if (!String.IsNullOrEmpty(record.sex))
                    
                    {
                        db = db.Where<sys_user>(p => p.sex.Equals(record.sex));
                    }
                
                    if (!String.IsNullOrEmpty(record.avatar))
                    
                    {
                        db = db.Where<sys_user>(p => p.avatar.Equals(record.avatar));
                    }
                
                    if (!String.IsNullOrEmpty(record.password))
                    
                    {
                        db = db.Where<sys_user>(p => p.password.Equals(record.password));
                    }
                
                    if (!String.IsNullOrEmpty(record.status))
                    
                    {
                        db = db.Where<sys_user>(p => p.status.Equals(record.status));
                    }
                
                    if (!String.IsNullOrEmpty(record.del_flag))
                    
                    {
                        db = db.Where<sys_user>(p => p.del_flag.Equals(record.del_flag));
                    }
                
                    if (!String.IsNullOrEmpty(record.login_ip))
                    
                    {
                        db = db.Where<sys_user>(p => p.login_ip.Equals(record.login_ip));
                    }
                
                    if (record.login_date != null)
                    
                    {
                        db = db.Where<sys_user>(p => p.login_date.Equals(record.login_date));
                    }
                
                    if (!String.IsNullOrEmpty(record.create_by))
                    
                    {
                        db = db.Where<sys_user>(p => p.create_by.Equals(record.create_by));
                    }
                
                    if (record.create_time != null)
                    
                    {
                        db = db.Where<sys_user>(p => p.create_time.Equals(record.create_time));
                    }
                
                    if (!String.IsNullOrEmpty(record.update_by))
                    
                    {
                        db = db.Where<sys_user>(p => p.update_by.Equals(record.update_by));
                    }
                
                    if (record.update_time != null)
                    
                    {
                        db = db.Where<sys_user>(p => p.update_time.Equals(record.update_time));
                    }
                
                    if (!String.IsNullOrEmpty(record.remark))
                    
                    {
                        db = db.Where<sys_user>(p => p.remark.Equals(record.remark));
                    }
                
                }
                return db.ToList();
             }
        }

        /// <summary>
        /// 分页查询
        /// </summary>
        public PageInfo<sys_user> queryPage(PageInfo<sys_user> page)
        {

            Expression<Func<sys_user, bool>> whereLambda = PredicateExtensions.True<sys_user>();
            sys_user record = page.queryParams;
            if (record != null)
            {          
                if (record.user_id != null)
                
                {
                    whereLambda.And(p => p.user_id.Equals(record.user_id));
                }
            
                if (record.dept_id != null)
                
                {
                    whereLambda.And(p => p.dept_id.Equals(record.dept_id));
                }
            
                if (!String.IsNullOrEmpty(record.user_name))
                
                {
                    whereLambda.And(p => p.user_name.Equals(record.user_name));
                }
            
                if (!String.IsNullOrEmpty(record.nick_name))
                
                {
                    whereLambda.And(p => p.nick_name.Equals(record.nick_name));
                }
            
                if (!String.IsNullOrEmpty(record.user_type))
                
                {
                    whereLambda.And(p => p.user_type.Equals(record.user_type));
                }
            
                if (!String.IsNullOrEmpty(record.email))
                
                {
                    whereLambda.And(p => p.email.Equals(record.email));
                }
            
                if (!String.IsNullOrEmpty(record.phonenumber))
                
                {
                    whereLambda.And(p => p.phonenumber.Equals(record.phonenumber));
                }
            
                if (!String.IsNullOrEmpty(record.sex))
                
                {
                    whereLambda.And(p => p.sex.Equals(record.sex));
                }
            
                if (!String.IsNullOrEmpty(record.avatar))
                
                {
                    whereLambda.And(p => p.avatar.Equals(record.avatar));
                }
            
                if (!String.IsNullOrEmpty(record.password))
                
                {
                    whereLambda.And(p => p.password.Equals(record.password));
                }
            
                if (!String.IsNullOrEmpty(record.status))
                
                {
                    whereLambda.And(p => p.status.Equals(record.status));
                }
            
                if (!String.IsNullOrEmpty(record.del_flag))
                
                {
                    whereLambda.And(p => p.del_flag.Equals(record.del_flag));
                }
            
                if (!String.IsNullOrEmpty(record.login_ip))
                
                {
                    whereLambda.And(p => p.login_ip.Equals(record.login_ip));
                }
            
                if (record.login_date != null)
                
                {
                    whereLambda.And(p => p.login_date.Equals(record.login_date));
                }
            
                if (!String.IsNullOrEmpty(record.create_by))
                
                {
                    whereLambda.And(p => p.create_by.Equals(record.create_by));
                }
            
                if (record.create_time != null)
                
                {
                    whereLambda.And(p => p.create_time.Equals(record.create_time));
                }
            
                if (!String.IsNullOrEmpty(record.update_by))
                
                {
                    whereLambda.And(p => p.update_by.Equals(record.update_by));
                }
            
                if (record.update_time != null)
                
                {
                    whereLambda.And(p => p.update_time.Equals(record.update_time));
                }
            
                if (!String.IsNullOrEmpty(record.remark))
                
                {
                    whereLambda.And(p => p.remark.Equals(record.remark));
                }
            
            }
            LoadPageItems(page, whereLambda, p => p.user_id, true);
            return page;

            // return LoadPageItems(5, 2, out _total, whereLambda, p => p.id, true);
        }

        /// <summary>
        /// 删除
        /// </summary>
        public int deleteById(int id)
        {
            using (pwEntities myDb = new pwEntities())
            {
                // TODO 生成代码后需要检查一下是否找到正确的主键，这里做一个错误代码，避免直接使用
                sys_user record = new sys_user() { user_id = id };
                    
                myDb.sys_user.Attach(record);
                myDb.Entry(record).State = EntityState.Deleted;
                return myDb.SaveChanges();
            }
        }

        /// <summary>
        /// 添加
        /// </summary>
        public int add(sys_user record)
        {
            using (pwEntities myDb = new pwEntities())
            {
                myDb.sys_user.Add(record);
                return myDb.SaveChanges();
            }
        }

        /// <summary>
        /// 更新
        /// </summary>
        public int updateById(sys_user record)
        {
            using (pwEntities myDb = new pwEntities())
            {
                myDb.sys_user.Attach(record);
                myDb.Entry(record).State = EntityState.Modified;
                return myDb.SaveChanges();
            }
        }

        /// <summary>
        /// getById
        /// </summary>
        public sys_user getById(int id)
        {
            using (pwEntities myDb = new pwEntities())
            {
                 // TODO 生成代码后需要检查一下是否找到正确的主键，这里做一个错误代码，避免直接使用
              
                 return myDb.Set<sys_user>().Where<sys_user>(p => p.user_id == id).FirstOrDefault<sys_user>();
                  
            }
        }

        public sys_user getByUserName(string username)
        {
            using (pwEntities myDb = new pwEntities())
            {
                return myDb.Set<sys_user>().Where<sys_user>(p => p.user_name == username).FirstOrDefault<sys_user>();
            }
        }
    }
  }
