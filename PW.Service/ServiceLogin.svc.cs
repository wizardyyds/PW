﻿using PW.DBCommon.Dao;
using PW.DBCommon.Model;
using PW.Service.common;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.Text;

namespace PW.Service
{
    // 注意: 使用“重构”菜单上的“重命名”命令，可以同时更改代码、svc 和配置文件中的类名“Service1”。
    // 注意: 为了启动 WCF 测试客户端以测试此服务，请在解决方案资源管理器中选择 Service1.svc 或 Service1.svc.cs，然后开始调试。
    public class ServiceLogin : IServiceLogin
    {
        public ServiceResult<sys_user> Login(string username, string pwd)
        {
           
            sys_user user = new SysUserDao().getByUserName(username);
            if (user == null)
            {
                return ServiceResult<sys_user>.Error(String.Format("登录用户：{0} 不存在", username), new sys_user());
            }
            else if ("2".Equals(user.del_flag))
            {

                return ServiceResult<sys_user>.Error(String.Format("对不起，您的账号：{0} 已被删除", username), new sys_user());
            }
            else if ("1".Equals(user.status))
            {
                return ServiceResult<sys_user>.Error(String.Format("对不起，您的账号：{0} 已停用", username), new sys_user());
            }
            else {
                // string salt = BCrypt.Net.BCrypt.GenerateSalt(10);
                // string passwordHash = BCrypt.Net.BCrypt.HashPassword(pwd, salt);
                if (BCrypt.Net.BCrypt.Verify(pwd, user.password))
                {
                    user.password = "";
                    return ServiceResult<sys_user>.Success("登录成功", user);
                }
                else {

                    return ServiceResult<sys_user>.Error("登录失败，账号或者密码错误！", new sys_user());
                }
            } 
        }

        public UserInfo GetUserInfo(string userid)
        {
            return new UserInfo() { username="root",fullname="管理员", role="admin",userid="1" };
        }
    }
}
