using PW.DBCommon.Dao;
  using PW.DBCommon.Model;
  using System.Collections.Generic;

  namespace PW.Service
  {
    public class ServiceSysMenu : IServiceSysMenu
    {
        /// <summary>
        /// 查询
        /// </summary>
        public List<sys_menu> query(sys_menu record)
        {
            return new SysMenuDao().query(record);
        }
 
        /// <summary>
        /// 分页查询
        /// </summary>
        public PageInfo<sys_menu> queryPage( PageInfo<sys_menu> record)
        {
            return new SysMenuDao().queryPage(record);
        }

        /// <summary>
        /// 删除
        /// </summary>
        public int deleteById(long id)
        {
            return new SysMenuDao().deleteById(id);
        }

        /// <summary>
        /// 添加
        /// </summary>
        public int add(sys_menu record)
        {
            return new SysMenuDao().add(record);
        }

        /// <summary>
        /// 更新
        /// </summary>
        public int updateById(sys_menu record)
        {
            return new SysMenuDao().updateById(record);
        }

        /// <summary>
        /// getById
        /// </summary>
        public sys_menu getById(long id)
        {
            return new SysMenuDao().getById(id);
        }
    }
  }